import numpy as np                          #Matrix ops
import matplotlib.pyplot as plt             #Plots

#Symmetrieoperationen Matrixdarstellung
sym_matrices = {
         '1': [[1, 0, 0],
               [0, 1, 0],
               [0, 0, 1]],
        '-1': [[-1, 0, 0],
               [0, -1, 0],
               [0, 0, -1]],
        'mx': [[-1, 0, 0],
               [0, 1, 0],
               [0, 0, 1]],
        'my': [[1, 0, 0],
               [0, -1, 0],
               [0, 0, 1]],
        'mz': [[1, 0, 0],
               [0, 1, 0],
               [0, 0, -1]],
        '2x': [[1, 0, 0],
               [0, -1, 0],
               [0, 0, -1]],
        '2y': [[-1, 0, 0],
               [0, 1, 0],
               [0, 0, -1]],
        '2z': [[-1, 0, 0],
               [0, -1, 0],
               [0, 0, 1]],
}

#Eingabe der Koordinaten
p      = input('Koordinaten des Punkts; z.B. 0.1 0.2 0.3 oder "Enter" für 0.1 0.2 0.3: ')
pxyz_0 = np.asarray([float(p) for p in p.split()])

if p == '':
    pxyz_0 = np.array([0.1, 0.2, 0.3])

#Eingabe des Symmetrieelements
sym_el = input('Symmmetrieelement; Auswahl: 1, -1, 2x, 2y, 2z, mx, my, mz oder "Enter" für 1: ')
sym_op = sym_matrices.get(str(sym_el))

if sym_el == '':
    sym_op = sym_matrices.get(str('1'))

#Eingabe der Translationskomponente
t = input('Translation; z.B. 0.5 1 -0.5 oder "Enter" für keine Translation: ')
trans = np.asarray([float(t) for t in t.split()])

if t == '':
    trans = np.array([0, 0, 0])

print(' ')

#Ausgabe der Symmetrioperation als Matrix
print('Symmetrieoperation als Matrix:')
print(sym_op)

#Multiplikation der Punktkoordinaten mit Matrix
# x,y,z * M = x1,y1,z1
pxyz_1 = np.dot(pxyz_0, sym_op)

pxyz_1 = np.add(pxyz_1, trans)

print(' ')

#Ausgabe der Koordinaten vor Symmetrieoperation
print('Koordinaten vor Symmetrieoperation:')
print(pxyz_0)

#Ausgabe der Koordinaten nach Symmetrieoperation
print('Koordinaten nach Symmetrieoperation:')
print(pxyz_1)

#Grafische Darstellung
fig = plt.figure()
ax  = fig.add_subplot(projection='3d')
ax.scatter(0,0,0, color='black')
ax.text(0,0,0, '0,0,0')

ax.stem([pxyz_0[0]],[pxyz_0[1]],[pxyz_0[2]], linefmt = 'b', markerfmt ='o')
ax.text(pxyz_0[0],pxyz_0[1],pxyz_0[2], str(pxyz_0))

ax.stem([pxyz_1[0]],[pxyz_1[1]],[pxyz_1[2]], linefmt ='r', markerfmt ='o')
ax.text(pxyz_1[0],pxyz_1[1],pxyz_1[2], str(pxyz_1))

ax.set_xlabel('x', fontsize=20)
ax.set_ylabel('y', fontsize=20)
ax.set_zlabel('z', fontsize=20)

plt.tight_layout()
plt.show()